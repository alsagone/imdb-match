# imdb-match

> Utilise une API d&#39;IMDb pour recouper le cast de deux séries ou films différent·e·s pour pouvoir trouver s&#39;il y a un ou plusieurs acteur·trice·s qui a/ont joué dans les deux

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
